/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Button, FlatList} from 'react-native';
import addStudent from './redux/actions/student';
import { connect } from 'react-redux';


class App extends Component {

  state = {
    studentName: '',
    class: []
  }

  changeText = (value)=>{
    this.setState({studentName:value})
  }

  addStudentToClass = () => {
    
    this.props.add(this.state.studentName, true)
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          placeholder = "cho son 2"
          value = {this.state.studentName}
          onChangeText = {this.changeText}
        >
        </TextInput>
        <Button 
          title = "cho son"
          onPress = {this.addStudentToClass}
        > 
        </Button>
        <FlatList
          data={this.props.class}
          renderItem={({item}) => <Text>{item.name} + "    " + {item.status == true ? "Có mặt" : "Vắng mặt"}</Text>}
        />
      </View>
    );0 
  }
}
const mapStateToProps = state => {
  return{
    class: state.student.class,
  }
};
const mapDispatchToProps = dispatch => {
  return{
    add: (name, status) => {
        dispatch(addStudent(name,status))
    }
  }
};
export default connect(mapStateToProps , mapDispatchToProps)(App);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
