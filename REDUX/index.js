/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import configureStore from './redux/store';
import React from 'react'

const store = configureStore();

const RNApp = ()=>{
    return(
        <Provider store = {store}>
            <App/>
        </Provider>

    );
} 


AppRegistry.registerComponent(appName, () => RNApp);
