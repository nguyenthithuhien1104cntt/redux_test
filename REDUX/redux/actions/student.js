import {ADD_STUDENT} from './type';
const addStudent = (name, status) =>{
    return {
        type: ADD_STUDENT,
        name: name,
        status: status,
    }
};
export default addStudent;
 