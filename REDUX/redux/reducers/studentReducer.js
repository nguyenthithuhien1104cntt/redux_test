import {ADD_STUDENT} from '../actions/type'
const init = {
    class:[]
};

const studentReducer = (state = init, action) => {
       
        switch (action.type){
            case ADD_STUDENT:
                return {
                    ...state,
                    class: state.class.concat(
                        {
                            id: Math.random(),
                            name: action.name,
                            status: action.status
                        }
                    )
                };
            default:
                return state;
        }
};
export default studentReducer;